'use strict';

/**
 * @ngdoc overview
 * @name yuGiOhApp
 * @description
 * # yuGiOhApp
 *
 * Main module of the application.
 */
angular
  .module('yuGiOhApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainController'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

