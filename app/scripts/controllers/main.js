'use strict';

/**
 * @ngdoc function
 * @name yuGiOhApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yuGiOhApp
 */
angular.module('yuGiOhApp')
    .controller('MainController', ['$scope', '$rootScope', function($scope, $rootScope) {
        $scope.cardNames = [
            'Burial from a Different Dimension',
            'Charge of the Light Brigade',
            'Infernoid Antra',
            'Infernoid Attondel',
            'Infernoid Decatron',
            'Infernoid Devyaty',
            'Infernoid Harmadik',
            'Infernoid Onuncu',
            'Infernoid Patrulea',
            'Infernoid Pirmais',
            'Infernoid Seitsemas',
            'Lyla, Lightsworn Sorceress',
            'Monster Gate',
            'One for One',
            'Raiden, Hand of the Lightsworn',
            'Reasoning',
            'Time­Space Trap Hole',
            'Torrential Tribute',
            'Upstart Goblin',
            'Void Seer'
        ];
        $scope.cards = [];
        $scope.selectedCard = false;
        $scope.selectCard = function(card) {
            $scope.selectedCard = card;
            if(!$scope.selectedCard.image) {
                $scope.selectedCard.image = "http://yugiohprices.com/api/card_image/" + $scope.selectedCard.name;
            }
        };

        function createCardEntities(element, index) {
            var _carData = {
                name: element,
                type: 'unknown',
                selectCard: $scope.selectCard
            };
            $.get('https://jsonp.afeld.me/?url=http://yugiohprices.com/api/card_data/' + element)
            .done(function(resp) {
                if(resp.data) {
                    console.log(resp);
                    _carData.type = resp.data.card_type;
                    _carData.text = resp.data.text;
                    $scope.cards.push(_carData);
                    $scope.$apply();
                }
            });
        }

        $scope.cardNames.forEach(createCardEntities);


    }]);
